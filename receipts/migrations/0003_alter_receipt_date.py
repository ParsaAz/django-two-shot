# Generated by Django 4.2.1 on 2023-06-02 22:34

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_rename_owener_account_owner_alter_receipt_account"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(),
        ),
    ]
