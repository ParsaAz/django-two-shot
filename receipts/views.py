from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CreateCategory, CreateAccount
from django.contrib.auth.models import User
# Create your views here.


@login_required
def receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipts": receipts
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            new_category.save()
            return redirect("category_list")

    else:
        form = CreateCategory

    context = {
        "form": form,
    }
    return render(request, 'categories/create.html', context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccount

    context = {
        "form": form,
    }
    return render(request, 'accounts/create.html', context)
